import * as express from 'express';
import { graphqlHTTP } from 'express-graphql';
import { ContinentsController } from './controllers/ContinentsController';
import { CountriesController } from './controllers/CountriesController';
import { LanguagesController } from './controllers/LanguagesController';
import { schema } from './utils/Schemas';
import bodyparser = require ('body-parser');
export class Server {

  public testing: any;
  public app: express.Application = express();
  public router: express.Application = express.Router();

  public schema = schema;

  constructor() {
    this.SetData();
    this.setRoutes();
  }

  public async SetData() {
    await ContinentsController.continentsSetData();
    await LanguagesController.languagesSetData();
    await CountriesController.countriesSetData();
  }

  setRoutes() {
    this.router.use(bodyparser.json());
    this.router.use('/graphql', graphqlHTTP({
      schema: this.schema,
      graphiql: true,
    }));

    this.app.use('/.netlify/functions/api',this.router);
  }

}

