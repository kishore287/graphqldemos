import {Server} from './server';
import serverless = require('serverless-http');

let server = new Server().app;

export const handler = serverless(server);